#commencer par installer pygame, pytmx pour la carte et pyscroll pour controller le zoom et le deplacement de camera
import pygame
import pyscroll
from pyscroll.group import PyscrollGroup
import pytmx
from pytmx import util_pygame
from player import Player

#initialiser le module pygame
class Game:
    def __init__(self):
        #creer la fenetre du jeu et definir la resolution
        self.screen = pygame.display.set_mode((800, 800))
        pygame.display.set_caption("PyGame")

        #charger la carte en format .tmx
        tmx_data = pytmx.util_pygame.load_pygame('PyGameMap.tmx')
        map_data = pyscroll.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        #generer la joueur sur l'objet (player spawn defini sur TILED)
        player_position = tmx_data.get_object_by_name("player_spawn")
        self.player = Player(player_position.x, player_position.y)

        #liste des rectangle de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))#ajouter a la liste des murs(walls)

        #dessiner le groupe de calques
        self.group = pyscroll.PyscrollGroup(map_layer = map_layer, default_layer = 5)
        self.group.add(self.player)

    def handle_input(self):
        pressed = pygame.key.get_pressed()

        if pressed[pygame.K_UP]:
            self.player.move_up()
            self.player.change_animation('up')
        
        elif pressed[pygame.K_DOWN]:
            self.player.move_down()
            self.player.change_animation('down')
        
        elif pressed[pygame.K_RIGHT]:
            self.player.move_right()
            self.player.change_animation('right')
        
        elif pressed[pygame.K_LEFT]:
            self.player.move_left()
            self.player.change_animation('left')

    #fonction qui fait le travail d'actaulisation du groupe
    def update(self):
        self.group.update()

        #verification de collsion ave une boucle
        for sprite in self.group.sprites():
            if sprite.feet.collidelist(self.walls) > -1:
                sprite.move_back()

    def run(self):
        clock = pygame.time.Clock()

        #Boucle du jeu
        jeuEnCours = True
        #on recupere tous les elements actifs du jeu et s'il detecte que pygame.QUIT est active il va quitter le jeu
        while jeuEnCours:

            self.player.save_location()
            self.handle_input()
            self.update()
            self.group.center(self.player.rect)#centrer la camera sur le joueur et le suivre
            self.group.draw(self.screen)
            pygame.display.flip()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    jeuEnCours = False
            
            clock.tick(60)
        
        pygame.quit()
