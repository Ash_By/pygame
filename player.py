import pygame
import animation

class Player(pygame.sprite.Sprite):
#class Player(animation.AnimateSprite):
    #recup du sprite sheet du joueur
    def __init__(self, x, y):
        super().__init__()#pour initialiser le sprite sheet (player.png)
        #super().__init__("Player.png")
        self.sprite_sheet = pygame.image.load("assets/Player.png")

        self.image = self.get_image(0, 0)
        self.image.set_colorkey([0, 0, 0])#background
        self.rect = self.image.get_rect()#rectangle qui est la position du sprite sheet()ou placer la joueur
        self.position = [x, y]
        self.images = {
        "down" : self.get_image(0, 0),
        "left" : self.get_image(0, 32),
        "right" :  self.get_image(0, 64),
        "up" : self.get_image(0, 96)
        }
        self.feet = pygame.Rect(0, 0, self.rect.width * 0.5, 12)
        self.old_postion = self.position.copy()
        self.speed = 2

    def save_location (self):self.old_position = self.position.copy()

    def change_animation(self, name) : #self.image = self.images[name] # a la meme ligne si il y a une seule instruction
        self.image = self.images[name]
        self.image.set_colorkey([0, 0, 0])
    
    
    def move_right(self): self.position[0] += self.speed

    def move_left(self): self.position[0] -= self.speed

    def move_up(self): self.position[1] -= self.speed

    def move_down(self): self.position[1] += self.speed

    def update(self):
        self.rect.topleft = self.position
        self.feet.midbottom = self.rect.midbottom

    #replacer le joueur a la postion avant la collision
    def move_back (self):
        self.postion = self.old_position
        self.rect.topleft = self.position
        self.feet.midbottom = self.rect.midbottom

    #methode qui va donner les coordonnees en x y de l'image en question
    def get_image(self, x, y):
        image = pygame.Surface([32, 32])#on precise la taille de l'image qu'on va charger vu que limage se compose de plusieurs personnages
        image.blit(self.sprite_sheet, (0,0), (x, y, 32, 32))#image.blit va aller absorber l'image en question
        return image